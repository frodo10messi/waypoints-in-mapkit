//
//  ViewController.swift
//  mapkit_morethan2points
//
//  Created by SGI-Mac7 on 27/11/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController ,MKMapViewDelegate{

    @IBOutlet weak var MapV: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        MapV.delegate = self
        
        manycoordinates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func manycoordinates (){
        
        direction(indez: 0, time: 0,routes: [])
       
        
    }
    func direction(indez :Int,time:TimeInterval,routes:[MKRoute]){
        
        let locations:[CLLocationCoordinate2D] = [CLLocationCoordinate2DMake(40.759011, -73.984472),CLLocationCoordinate2DMake(40.748441, -73.985564),CLLocationCoordinate2DMake(40.759021, -73.989874),CLLocationCoordinate2DMake(40.78451, -73.985579),CLLocationCoordinate2DMake(40.79000,
            -73.983472)
        ]
        let locationTitles:[String] = ["fk10","fk9","fk8","fk11","fk12"]
        
        let sourcePlacemark = MKPlacemark(coordinate: locations[indez], addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: locations[indez+1], addressDictionary: nil)
        
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = locationTitles[indez]
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = locationTitles[indez+1]
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        self.MapV.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) in
            if let response = response?.routes{
                let sortedDistance = response.sorted(by: {$0.distance < $1.distance })
                    
                if  (sortedDistance.count) > 0  {
                    let quickestRouteForSegment: MKRoute = sortedDistance[0]
                    
                    
                    var timeVar = time
                    var routesVar = routes
                    
                    routesVar.append(quickestRouteForSegment)
                    timeVar += quickestRouteForSegment.expectedTravelTime
                    print(indez)
                    if indez+2 < locations.count {
                        self.direction(indez: indez+1, time: timeVar, routes: routesVar)
                    } else {
                        self.showRoute(routes: routesVar, time: timeVar)
                        
                    }
                    
                    
                    // 
                    //        directions.calculate {
                    //            (response, error) -> Void in
                    
                    //            guard let response = response else {
                    //                if let error = error {
                    //                    print("Error: \(error)")
                    //                }
                    //
                    //                return
                    //            }
                    
                    
                    
                    //            print(response.routes.count)
                    //            let route = response.routes[0]
                    //            self.MapV.add((route.polyline), level: MKOverlayLevel.aboveRoads)
                    //            print("testing")
                    //            let rect = route.polyline.boundingMapRect
                    //            self.MapV.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
                }
                
                
                
                
                
            }

          
                
            }
            
            
            
            
            
            
            
            
            
        }
        
    
        
    func showRoute(routes: [MKRoute], time: TimeInterval) {
            for i in 0..<routes.count {
                plotPolyline(route: routes[i])
            }
            
        }
        func plotPolyline(route: MKRoute) {
            
            MapV.add(route.polyline)
            if MapV.overlays.count == 1 {
                MapV.setVisibleMapRect(route.polyline.boundingMapRect,
                                          edgePadding: UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0),
                                          animated: false)
            } else {
                let polylineBoundingRect =  MKMapRectUnion(MapV.visibleMapRect,
                                                           route.polyline.boundingMapRect)
                MapV.setVisibleMapRect(polylineBoundingRect,
                                          edgePadding: UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0),
                                          animated: false)
            }
           
        }
        
        
    func mapView(_ MapV: MKMapView,
                 rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if (overlay is MKPolyline) {
            if MapV.overlays.count == 1 {
                polylineRenderer.strokeColor =
                    UIColor.orange.withAlphaComponent(0.75)
            } else if MapV.overlays.count == 2 {
                polylineRenderer.strokeColor =
                    UIColor.green.withAlphaComponent(0.75)
            } else if MapV.overlays.count == 3 {
                polylineRenderer.strokeColor =
                    UIColor.black.withAlphaComponent(0.75)
            }
            else if MapV.overlays.count == 4 {
                polylineRenderer.strokeColor =
                    UIColor.blue.withAlphaComponent(0.75)
            }
            polylineRenderer.lineWidth = 5
        }
        return polylineRenderer
    }
        
        
        
}

